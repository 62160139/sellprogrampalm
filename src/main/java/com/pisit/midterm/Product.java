/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pisit.midterm;

/**
 *
 * @author User
 */
import java.io.Serializable;
public class Product implements Serializable{

    private String id;
    private String name;
    private String brand;
    private double price;
    private int amount;
    
    public Product(String id, String name, String brand, double price, int amount){
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.amount = amount;
    }

    Product() {
        
    }

    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    
    public String toString(){
        return "ID : " + id + " , " + "Name : " + name + " , "
                + "Brand : " + brand + " , " + 
                "Price : " + price + " , " + "Amount : " + amount;
    }
    
}
