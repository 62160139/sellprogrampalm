/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pisit.midterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Input {
    
    private static ArrayList<Product> sellList = new ArrayList<>();
    
    static {
        sellList = new ArrayList<>();
        
        sellList.add(new Product("1","Rice","easy",15.0,1));
        sellList.add(new Product("2","Sausage","Ceepee",35.0,1));
        sellList.add(new Product("3","Snack","ArhanYodkhun",10.0,2));
    }
    
    
    static{
        load();
    }
    
    public static void Clear(){
        sellList.clear();
        save();
    }
    
    public static String Cal(){
        double sum = 0.0;
        int result = 0;
        for(int i=0; i<sellList.size(); i++){
            sum += sellList.get(i).getPrice();
            result+= sellList.get(i).getAmount();
        }
        return "Total : " + sum + " Bath " + "Total : " + result + " Items";
    }
    
    public static boolean addProduct(Product user){
        sellList.add(user);
        save();
        return true;
    }
    
    public static boolean delUser(Product user){
        sellList.remove(user);
        save();
        return true;
    }
    
    public static boolean delUser(int index){
        sellList.remove(index);
        save();
        return true;
    }
    
    public static ArrayList<Product> getUsers(){
        return sellList;
    }
    
    public static boolean updateUser(int index, Product user){
        sellList.set(index, user);
        save();
        return true;
    }
    
    public static Product getUser(int index){
        return sellList.get(index);
    }
    
    
    
    public static void save(){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("Palm.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(sellList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Input.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Input.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void load(){
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("Palm.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            sellList = (ArrayList<Product>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Input.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Input.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Input.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    static void addUser(Product user) {
        
    }
    
    
}
